﻿using UnityEngine;
using System.Collections.Generic;

public class ThumbnailManager:MonoBehaviour
{
	[Header("Settings"),SerializeField] protected Vector2 _cellSize;
	[SerializeField] protected Vector2 _cellSpacing;
	[SerializeField] protected int _columns = 5;
	[SerializeField] protected int _totalItems = 1000;
	[Header("References"), SerializeField] protected RectTransform _viewport;
	[SerializeField] protected Camera _camera;
	[SerializeField] protected RectTransform _viewportExtruded;
 	public Transform container;
	public GameObject prefab;

	[SerializeField] private List<ThumbnailVO> _thumbnailVOList = new List<ThumbnailVO>();
	[SerializeField] protected Transform _poolContainer;
	
	void Start ()
	{
		Application.targetFrameRate = 60;
		createThumbnailVOList();
		setPanelSize();
		UpdatedScroll();
	}

	private void createThumbnailVOList() {
		ThumbnailVO thumbnailVO;
		float x = (_cellSize.x/2);
		float y = (_cellSize.y/2);
		for (int i=0; i<_totalItems; i++) {
			thumbnailVO = new ThumbnailVO();
			thumbnailVO.localPos = new Vector2(x, -y);
			if ((i+1) % _columns == 0)
			{
				x =  (_cellSize.x/2);
				y += (_cellSize.y + _cellSpacing.y);
			}
			else
			{
				x += (_cellSize.x + _cellSpacing.x);
			}
			thumbnailVO.id = i.ToString();
            _thumbnailVOList.Add(thumbnailVO);
        }
	}
	
	private void setPanelSize()
	{
		int totalLines = _totalItems / _columns;
		var contentPanel = container as RectTransform;
		float totalHeight = totalLines * (_cellSize.y + _cellSpacing.y);
		contentPanel.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, totalHeight);
	}

	public void UpdatedScroll()
	{
		bool changed = false;
		for (int i = 0; i < _totalItems; i++)
		{
			// we only check for the first one in each line
			if (i % _columns == 0)
			{
				bool oldVisible = _thumbnailVOList[i].visible;
				var p = container.TransformPoint(_thumbnailVOList[i].localPos);
				Vector2 sp = RectTransformUtility.WorldToScreenPoint(_camera, p);
				bool insideView = RectTransformUtility.RectangleContainsScreenPoint(_viewportExtruded, sp, _camera);
				// we check if visibility changed
				if (insideView != oldVisible)
					changed = true;
				// now we apply the resulting visibility flag in all the line
				for (int i2 = 0; i2 < _columns; i2++)
				{
					if (_thumbnailVOList.Count > i + i2)
					{
						_thumbnailVOList[i + i2].visible = insideView;
					}
				}
			}
		}
		// we only refresh prefabs when visibility values changed
		if (changed)
			RefreshPrefabs();
	}

	protected void RefreshPrefabs()
	{
		for (int i = 0; i < _totalItems; i++)
		{
			if (_thumbnailVOList[i].visible && _thumbnailVOList[i].obj == null)
			{
				CreateThumbnail(_thumbnailVOList[i]);
			}
			else if (!_thumbnailVOList[i].visible && _thumbnailVOList[i].obj != null)
			{
				DestroyThumbnail(_thumbnailVOList[i]);
			}
		}
	}

	private void DestroyThumbnail(ThumbnailVO data)
	{
		GameObject gameObj = data.obj.gameObject;
		gameObj.SetActive(false);
		gameObj.transform.SetParent(_poolContainer);
		data.obj = null;
		_pool.Push(gameObj);
	}

	private void CreateThumbnail(ThumbnailVO data)
	{
		GameObject gameObj = null;
		if (_pool.Count > 0)
		{
			gameObj = _pool.Pop();
		}
		else
		{
			gameObj = (GameObject) Instantiate(prefab);	
		}
		gameObj.transform.SetParent(container, false);
		gameObj.GetComponent<Thumbnail>().thumbnailVO = data;
		gameObj.transform.localPosition = data.localPos;
		(gameObj.transform as RectTransform).sizeDelta = new Vector2(_cellSize.x, _cellSize.y);
		data.obj = gameObj.transform as RectTransform;
		gameObj.SetActive(true);
	}

	protected Stack<GameObject> _pool = new Stack<GameObject>();
}
