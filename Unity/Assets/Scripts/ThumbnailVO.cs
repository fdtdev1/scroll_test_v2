﻿using UnityEngine;

[System.Serializable]
public class ThumbnailVO {

	public Vector2 localPos;
	public string id;
	public RectTransform obj; //temp
	public bool visible;
	public ThumbnailVO() {}

}